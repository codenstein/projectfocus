const router =  require('express').Router()
let User = require('../models/user.model')
const bcrypt = require('bcryptjs')
require('dotenv').config()
const jwt = require('jsonwebtoken')
// const {JWT_SECRET} = require('../keys')
const JWT_SECRET = process.env.JWT_SECRET
//..............................................................................................................................

router.route('/').get((req, res)=>{
    User.find()
    .then(users=>res.json(users))
        .catch(err=>res.status(400).json('Error'+err))
});
//..............................................................................................................................

router.route('/add').post((req, res)=>{
    console.log("here")
    const username = req.body.name
    const name = req.body.name
    const email = req.body.email
    const password = req.body.password
    console.log(username +"\n"+ email +"\n"+password+"\n"+name)

    if(!email || !password || !username){
        return res.status(422).json({error:"please enter all the fields"})
    }
    else
    User.findOne({email:email})
        .then((savedUser)=>{
            if(savedUser){
                return res.status(422).json({error:"user already exists with that email"})
            }
            else
            bcrypt.hash(password,12)
                .then((hashedpassword)=>{
                    const user = new User({
                        name,
                        password:hashedpassword,
                        email,
                    })

                    user.save()
                        .then(user=>{
                            // transporter.sendMail({
                            //     to:user.email,
                            //     from:"no-reply@insta.com",
                            //     subject:"signup success",
                            //     html:"<h1>welcome to instagram</h1>"
                            // })
                            res.json({message:"saved successfully"})
                        })
                        .catch(err=>{
                            console.log(err)
                        })
                })

        })
        .catch(err=>{
            console.log(err)
        })
})

router.route('/signin').post((req, res)=>{
    const {email,password} = req.body
    if(!email || !password){
        return res.status(422).json({error:"please add email or password"})
    }
    User.findOne({email:email})
        .then(savedUser=>{
            if(!savedUser){
                return res.status(422).json({error:"Invalid Email or password"})
            }
            else
            bcrypt.compare(password,savedUser.password)
                .then(doMatch=>{
                    if(doMatch){
                        // res.json({message:"successfully signed in"})
                        const token = jwt.sign({_id:savedUser._id},JWT_SECRET)
                        const {_id,name,email} = savedUser
                        res.json({token,user:{_id,name,email}})
                    }
                    else{
                        return res.status(422).json({error:"Invalid Email or password"})
                    }
                })
                .catch(err=>{
                    console.log(err)
                })
        })
})


module.exports = router