const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000

app.use(cors())
app.use(express.json())

const uri = process.env.ATLAS_URI
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex:true, useUnifiedTopology:true})
    // .then(response=>(console.log('success')))
    // .catch(err=>console.log("error:"+err))

const connection = mongoose.connection

connection.on('error', console.error.bind(console, 'connection error:'));
connection.once('open', ()=>{
    console.log('successful')
})

const user = require('./routes/user')
app.use('/user', user)


app.listen(port, ()=>{
    console.log('server up and running on port: '+ port)
})

