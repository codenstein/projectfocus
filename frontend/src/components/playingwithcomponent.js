import {Component} from "react";

export default class Playingwithcomponent extends Component{
    state = {
        title: 'playing'
    }
    render() {
        return (
            <div>
                <h1>playing with component</h1>
                <h3>{this.state.title}</h3>
            </div>
        );
    }
}

