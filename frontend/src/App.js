import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"

import LandingPageComponent from "./components/landingPage.component";
import Playingwithcomponent from "./components/playingwithcomponent";

function App() {
  return (
    <Router>
       {/*<LandingPageComponent/>*/}
       <Playingwithcomponent/>
    </Router>
  );
}

export default App;
